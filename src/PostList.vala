class Deskit.PostList : Gtk.ListBox {
	private Gtk.ListBoxRow row;
	private Gtk.Spinner spinner;
	private Gtk.ListBoxRow spinner_row;

	private DMainWindow main_window;

	private const int MARGIN_START = 12;
	private const int MARGIN_END = 24;
	private const int MARGIN_HORIZONTAL = 8;

	private Util.ImgFetcher img_fetcher;

	public PostList(DMainWindow parent) {
		main_window = parent;
		row = get_selected_row();

		spinner = new Gtk.Spinner();
		spinner_row = new Gtk.ListBoxRow();
		spinner_row.add(spinner);
		img_fetcher = new Util.ImgFetcher();
	}

	public void show_posts(GLib.List<Data.Post> posts) {
		foreach (var post in posts) {
			var container = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
			container.halign = Gtk.Align.START;
			container.valign = Gtk.Align.CENTER;
			container.margin_top = MARGIN_HORIZONTAL;
			container.margin_bottom = MARGIN_HORIZONTAL;
			container.margin_start = MARGIN_START;
			container.margin_end = MARGIN_END;
				
			var text_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
			text_container.halign = Gtk.Align.START;
			text_container.valign = Gtk.Align.CENTER;

			var title = new Gtk.Label("<big>" + post.title + "</big>");
			title.use_markup = true;
			title.wrap = true;
			title.xalign = 0;
			title.halign = Gtk.Align.START;
			title.valign = Gtk.Align.START;

			var author = new Gtk.Label(post.author);
			author.halign = Gtk.Align.START;
			author.valign = Gtk.Align.END;

			var sub = new Gtk.Label(post.sub);
			sub.margin_top = MARGIN_HORIZONTAL;
			sub.halign = Gtk.Align.START;
			sub.valign = Gtk.Align.END;

			text_container.pack_start(title, true, true, 0);
			text_container.pack_end(author, true, true, 0);
			text_container.pack_end(sub, true, true, 0);

			if (post.thumbnail_uri != null) {
				var img = new Gtk.Image();
				container.pack_start(img, false, false, 0);
				img.valign = Gtk.Align.CENTER;
				img.halign = Gtk.Align.CENTER;
				text_container.margin_start = MARGIN_START;

				img_fetcher.get_image(
					post.thumbnail_uri,
					(data) => {
						var loader = new Gdk.PixbufLoader();

						try {
							loader.write(data);
							loader.close();
						} catch (GLib.Error e) {
							stderr.printf("Error loading image: %s\n",
										  e.message);
						} finally {
							var pixbuf = loader.get_pixbuf();
							img.set_from_pixbuf(pixbuf);
						}
					});
			}

			container.pack_start(text_container, true, true, 0);
			add(container);
		}
			
		show_all();
		connect_signals();
	}

	public void clear_posts() {
		var children = get_children();
		if ((int) children.length > 0) {
			foreach (var child in children) {
				child.destroy();
			}
		}
	}

	public void show_posts_spinner() {
		spinner.start();
		add(spinner_row);
		spinner_row.show();
		spinner.show();
	}

	public void hide_posts_spinner() {
		spinner.stop();
		remove(spinner_row);
		spinner_row.hide();
		spinner.hide();
	}

	private void connect_signals() {
		row_activated.connect(on_row_activated);
	}

	private void on_row_activated(Gtk.ListBoxRow activated_row) {
		var row_index = activated_row.get_index();
		main_window.activate_post(row_index);
		unselect_row(activated_row);
	}
}