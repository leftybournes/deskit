namespace Deskit.Util {
	delegate void CommentsLoadedCallback(string json_string);
}

class Deskit.Util.CommentsFetcher : Util.Fetcher {
	public void get_comments(string url, CommentsLoadedCallback cb) {
		var msg = new Soup.Message("GET", url);

		session.queue_message(msg, (sess, mess) => {
				stdout.printf("Firing request");
				cb((string) mess.response_body.data);

				mess.response_body.free();
				mess.response_headers.free();
				mess.request_body.free();
				mess.request_headers.free();
			});

		// session.send_message(msg);

		// cb((string) msg.response_body.data);

		// msg.response_body.free();
		// msg.response_headers.free();
		// msg.request_body.free();
		// msg.request_headers.free();
	}
}