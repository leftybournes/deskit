namespace Deskit.Util {
	delegate void ImageLoadedCallback(uint8[] data);
}

class Deskit.Util.ImgFetcher : Util.Fetcher {

	public void get_image(string url, owned ImageLoadedCallback cb) {
		var msg = new Soup.Message("GET", url);

		session.queue_message(msg, (sess, mess) => {
				cb(mess.response_body.data);
				
				msg.response_body.free();
				msg.response_headers.free();
				msg.request_body.free();
				msg.request_headers.free();
			});
	}
}
