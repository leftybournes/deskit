#!/usr/bin/env python3

import os
import subprocess

schemadir = os.path.join(os.environ['MESON_INSTALL_PREFIX'], 'share', 'glib-2.0',
                         'schemas')

if not os.environ.get('DESTDIR'):
    print('Compiling gsettings schemas...')
    subprocess.call(['glib-compile-schemas', schemadir])

    # print('Updating icon cache...')
    # icon_cache_dir = os.path.join(install_prefix, 'share/icons/hicolor')
    # subprocess.call(['gtk-update-icon-cache', '-qtf', icon_cache_dir])

    # print('Updating desktop database...')
    # desktop_database_dir = os.path.join(install_prefix, 'share/applications')
    # subprocess.call(['update-desktop-database', '-q', desktop_database_dir])
