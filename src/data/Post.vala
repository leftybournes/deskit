class Deskit.Data.Post {
	public string title;
	public string author;
	public string sub;
	public string permalink;
	public string url;
	public bool is_image;
	public bool is_video;

	public string? selftext;
	public string? thumbnail_uri;
}