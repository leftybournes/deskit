class Deskit.DMainWindow : Gtk.ApplicationWindow {
	private Gtk.Spinner content_spinner;
	private Gtk.Paned root_container;
	private Gtk.ScrolledWindow sidebar_scroll;
	private Gtk.ScrolledWindow posts_scroll;
	private Gtk.Stack stack;
	private Gtk.AccelGroup accel_group;

	private PostList post_list;
	private DHeaderBar headerbar;
	private Sidebar sidebar;
	private DApplication app;
	private PostView post_view;
	
	private const int WINDOW_WIDTH = 800;
	private const int WINDOW_HEIGHT = 600;
	private const int PANE_POSITION = 120;
	private const int SUB_ON_STARTUP = 0;

	private bool is_loading;
	private int current_sub;
	private string[] subs;
	private GLib.List<Data.Post> posts;

	private GLib.Settings settings;

	private Util.SubFetcher fetcher;
	// See reddit json api 'after'
	private string posts_after;

	public DMainWindow(DApplication app) {
		Object(application: app,
			   icon_name: "com.gitlab.kdeleteme.deskit");
		this.app = app;
	}

	construct {
		accel_group = new Gtk.AccelGroup();
		add_accel_group(accel_group);
		
		settings = new GLib.Settings(Strings.APPLICATION_ID);
		subs = settings.get_strv("subs");
			
		current_sub = SUB_ON_STARTUP;

		set_default_size(WINDOW_WIDTH, WINDOW_HEIGHT);
		headerbar = new DHeaderBar(this);
		set_titlebar(headerbar);
		headerbar.subtitle = subs[current_sub];

		root_container = new Gtk.Paned(Gtk.Orientation.HORIZONTAL);
		root_container.position = PANE_POSITION;
		
		build_sidebar();
		build_content_view();

		add(root_container);

		show_all();

		fetcher = new Util.SubFetcher();
		
		connect_signals();
		show_current_sub();
	}

	public void activate_sub(int sub_index) {
		if (sub_index != current_sub) {
			if (stack.visible_child_name == "post view") {
				headerbar.back_btn_deactivate();
			}

			if (posts_scroll.vadjustment.value > 0) {
				posts_scroll.vadjustment.value = 0.0;
			}

			content_spinner.start();
			stack.visible_child_name = "content spinner";

			current_sub = sub_index;
			headerbar.subtitle = subs[current_sub];

			sidebar.select_sub(current_sub);
			show_current_sub();
		}
	}

	public void hide_posts_spinner() {
		post_list.hide_posts_spinner();
	}

	public void activate_post(int row_index) {
		post_view.show_post(posts.nth_data(row_index));
		stack.visible_child_name = "post view";

		headerbar.back_btn_activate();
	}

	public void hide_post() {
		stack.visible_child_name = "posts";
	}

	public void add_sub_if_exists(string sub_name) {
		if (stack.visible_child_name == "post view") {
			headerbar.back_btn_deactivate();
		}

		content_spinner.start();
		stack.visible_child_name = "content spinner";

		fetcher.check_exists(sub_name, (exists) => {
				if (exists) {
					var new_sub = (int) subs.length;
					subs += sub_name;
					save_subs();
					update_subs();
					activate_sub(new_sub);
				} else {
					update_posts();
				}
			});
	}

	public void sort_subs(bool desc) {
		var front_page = subs[0];

		var subs_list = new GLib.List<string>();

		for (int i = 1; i < (int) subs.length; i++) {
			subs_list.append(subs[i]);
		}

		subs_list.sort((a, b) => {
				return strcmp(a.down(), b.down());
			});

		if (desc) {
			subs_list.reverse();
		}

		subs = {};
		subs += front_page;

		subs_list.foreach((sub) => {
				subs += sub;
			});

		save_subs();
		
		sidebar.show_sublist(subs);
	}

	public void delete_sub(uint sub_index) {
		var index = 0;

		string[] new_subs = {};
		foreach (var sub in subs) {
			if (index != sub_index) {
				new_subs += sub;
			}
			++index;
		}

		subs = new_subs[0:new_subs.length];
		save_subs();
		update_subs();
	}

	private void update_subs() {
		sidebar.show_sublist(subs);
	}

	private void build_sidebar() {
		sidebar = new Sidebar(this);
		sidebar.show_sublist(subs);
		
		sidebar_scroll = new Gtk.ScrolledWindow(null, null);
		sidebar_scroll.add(sidebar);
			
		root_container.pack1(sidebar_scroll, false, false);
	}

	private void build_content_view() {
		post_list = new PostList(this);

		posts_scroll = new Gtk.ScrolledWindow(null, null);
		posts_scroll.add(post_list);

		content_spinner = new Gtk.Spinner();

		post_view = new PostView();
			
		stack = new Gtk.Stack();
		stack.transition_type = Gtk.StackTransitionType.SLIDE_UP_DOWN;
		stack.add_named(posts_scroll, "posts");
		stack.add_named(post_view, "post view");
		stack.add_named(content_spinner, "content spinner");

		root_container.pack2(stack, true, false);

		content_spinner.start();
		stack.visible_child_name = "content spinner";
		content_spinner.show();
	}

	private void update_posts() {
		foreach (var child in post_list.get_children()) {
			child.destroy();
		}

		post_list.show_posts(posts);
		
		content_spinner.stop();
		stack.visible_child_name = "posts";
	}

	private void show_posts_spinner() {
		post_list.show_posts_spinner();
	}

	private void connect_signals() {
		posts_scroll.edge_overshot.connect(on_posts_scroll_bottom_overshot);
	}

	private void on_posts_scroll_bottom_overshot (Gtk.PositionType pos) {
		if (pos == Gtk.PositionType.BOTTOM) {
			load_more();
		}
	}

	private void load_more() {
		if (!is_loading) {
			is_loading = true;
			show_posts_spinner();

			string? sub_name = null;

			if (current_sub > 0) {
				sub_name = subs[current_sub];
			}

			fetcher.get_posts(sub_name, posts_after, (new_posts, after) => {
					post_list.show_posts(new_posts);
					hide_posts_spinner();
					is_loading = false;
					posts_after = after;

					foreach (var post in new_posts) {
						posts.append(post);
					}
				});
		}
	}

	private void show_current_sub() {
		if (!is_loading) {
			
			is_loading = true;
			posts = new GLib.List<Data.Post>();

			string? sub_name = null;

			if (current_sub > 0) {
				sub_name = subs[current_sub];
			}

			fetcher.get_posts(sub_name, null, (new_posts, after) => {
					post_list.clear_posts();
					post_list.show_posts(new_posts);
					content_spinner.stop();
					stack.visible_child_name = "posts";
					is_loading = false;

					posts_after = after;
					
					foreach (var post in new_posts) {
						posts.append(post);
					}
				});
		}
	}

	private void save_subs() {
		settings.set_strv("subs", subs);
	}
}