class Deskit.Sidebar : Gtk.ListBox {
	protected Gtk.ListBoxRow row;
	protected DMainWindow main_window;

	private const int MARGIN_START = 8;
		
	public Sidebar(DMainWindow parent) {
		Object();

		main_window = parent;
		row = get_selected_row();

		get_style_context().add_class(Gtk.STYLE_CLASS_SIDEBAR);
		border_width = 0;

		connect_signals();
	}

	public void show_sublist(string[] subs) {
		var children = get_children();
		foreach (var child in children) {
			child.destroy();
		}

		foreach (var sub in subs) {
			var listboxrow = new Gtk.ListBoxRow();
			var label = new Gtk.Label(sub);
			label.halign = Gtk.Align.START;
			label.margin_start = MARGIN_START;

			var event_box = new Gtk.EventBox();
			event_box.add(label);
			listboxrow.add(event_box);

			event_box.button_press_event.connect((event) => {
					      if (event.button == 3) {
					          var popover = new Gtk.Popover(event_box);
							  var popover_container = new Gtk.Box(
								  Gtk.Orientation.VERTICAL,
								  8);
					          var popover_delete = new Gtk.Button
							  .with_label("Delete");
							  popover_delete.relief = Gtk.ReliefStyle.NONE;
							  popover_container.pack_start(
								  popover_delete,
								  false,
								  false,
								  0);
					          popover.add(popover_container);
					          popover.popup();
					          popover.show_all();

							  popover_delete.clicked.connect(() => {
									  main_window.delete_sub(
										  listboxrow.get_index());
									  popover.popdown();
								  });

							  return true;
					      }
						  return false;
					  });

			add(listboxrow);
		}

		show_all();
	}

	public void select_sub(uint sub_index) {
		var row_to_select = get_row_at_index((int) sub_index);
		select_row(row_to_select);
	}

	private void connect_signals() {
		row_activated.connect(on_row_activated);
	}

	private void on_row_activated(Gtk.ListBoxRow activated_row) {
		var row_index = activated_row.get_index();
		main_window.activate_sub(row_index);
	}
}