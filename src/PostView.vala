class Deskit.PostView : Gtk.ScrolledWindow {
	private Gtk.Box container;
	private Gtk.Box comments_container;

	private const int COMMENT_DEFAULT_INDENT = 0;
	private const int COMMENT_SPACING = 24;
	private const int INDENTATION_STEP = 8;
	private const int MARGIN_START = 12;
	private const int MARGIN_END = 24;
	private const int MARGIN_HORIZONTAL = 12;
	private const int IMAGE_WIDTH_RESTRICTION = 600;

	private Util.ImgFetcher img_fetcher;
	private Util.CommentsFetcher comments_fetcher;

	public PostView() {
		Object(hadjustment: null, vadjustment: null);
	}

	construct {
		container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
		container.margin_top = MARGIN_HORIZONTAL;
		container.margin_bottom = MARGIN_HORIZONTAL;
		container.margin_start = MARGIN_START;
		container.margin_end = MARGIN_END;

		add(container);

		img_fetcher = new Util.ImgFetcher();
		comments_fetcher = new Util.CommentsFetcher();
	}

	public void show_post(Data.Post post) {
		clear();

		var title = new Gtk.Label("<big><b>" + post.title + "</b></big>");
		title.use_markup = true;
		title.wrap = true;
		title.halign = Gtk.Align.START;
		title.margin_bottom = MARGIN_HORIZONTAL;
		container.pack_start(title, false, false, 0);

		if (!post.is_image &&
			!(Strings.BASE_URL in post.url) &&
			(post.selftext == null || post.selftext.length == 0)) {
			var url = new Gtk.Label("<a href=\"" + post.url + "\">" +
									post.url + "</a>");
			url.use_markup = true;
			url.halign = Gtk.Align.START;
			url.xalign = 0;
			url.margin_bottom = MARGIN_HORIZONTAL;
			container.pack_start(url, false, false, 0);
		}
		
		if (post.is_image) {
			var img = new Gtk.Image();
			img.margin_bottom = MARGIN_HORIZONTAL;
			img.halign = Gtk.Align.START;

			var spinner = new Gtk.Spinner();
			var img_stack = new Gtk.Stack();
			img_stack.add(img);
			img_stack.add(spinner);
			container.pack_start(img_stack, false, false, 0);

			spinner.start();
			spinner.show();
			img_stack.visible_child = spinner;
			
			img_fetcher.get_image(post.url, (data) => {
					var loader = new Gdk.PixbufLoader();

					try {
						loader.write(data);
						loader.close();
					} catch (GLib.Error e) {
						stderr.printf("Error loading image: %s\n",
									  e.message);
					} finally {
						var pixbuf = loader.get_pixbuf();
						var height = pixbuf.height;
						var width = pixbuf.width;

						if (width > IMAGE_WIDTH_RESTRICTION) {
							var divisor = ((double) width /
										   (double) IMAGE_WIDTH_RESTRICTION *
										   100) / 100;
								
							height = (int) Math.floor((double) pixbuf.height /
													  divisor);
							width = (int) Math.floor((double) pixbuf.width /
													 divisor);
							pixbuf = pixbuf.scale_simple(width, height,
														 Gdk.InterpType
														 .BILINEAR);

						}
						spinner.stop();
						spinner.hide();
						img_stack.visible_child = img;

						img.set_from_pixbuf(pixbuf);
					}
				});
		}
		
		if (post.selftext != null && post.selftext.length > 0) {
			var selftext = new Gtk.Label(post.selftext);
			selftext.halign = Gtk.Align.START;
			selftext.xalign = 0;
			selftext.wrap = true;
			selftext.margin_bottom = MARGIN_HORIZONTAL;
			container.pack_start(selftext, false, false, 0);
		}

		var author = new Gtk.Label(post.author);
		author.halign = Gtk.Align.START;
		container.pack_start(author, false, false, 0);

		var sub = new Gtk.Label(post.sub);
		sub.halign = Gtk.Align.START;
		container.pack_start(sub, false, false, 0);

		show_comments(Strings.BASE_URL + post.permalink + ".json");

		container.show_all();
	}

	private void clear() {
		var post_children = container.get_children();
		if ((int) post_children.length > 0) {
			foreach (var child in post_children) {
				child.destroy();
			}
		}
	}

	private void show_comments(string url) {
		comments_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
		comments_container.margin_top = MARGIN_HORIZONTAL;
		container.pack_start(comments_container, false, false, 0);
		// var comments = GLib.List<Data.Comment>();

		comments_fetcher.get_comments(url, (json_string) => {
				var parser = new Json.Parser();

				try {
					parser.load_from_data(json_string);
				} catch (GLib.Error e) {
					stderr.printf("Error loading comments: %s\n", e.message);
				} finally {
					var node = parser.get_root().get_array();
					var comments_node = node.get_object_element(1);
					var comments_data = comments_node
					.get_object_member("data");
					var children = comments_data.get_array_member("children");

					parse_comments(children, COMMENT_DEFAULT_INDENT);
				}
			});
	}

	private void parse_comments(Json.Array json_comments, int depth) {
		// var elements = json_comments.get_elements();

		json_comments.foreach_element((array, index, node) => {
				var root = node.get_object();
				var data = root.get_object_member("data");
				var replies_object = data.get_object_member("replies");
			
				var comment_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
				comment_box.margin_start = depth;

				if (replies_object.has_member("data")) {
					var replies_data = replies_object.get_object_member("data");
					var replies_array = replies_data.get_array_member("children");
					parse_comments(replies_array, depth + INDENTATION_STEP);
				}

				if (data.has_member("author") && data.has_member("body")) {
					var author = new Gtk.Label("u/" + data.get_string_member("author"));
					author.margin_top = MARGIN_HORIZONTAL;
					author.halign = Gtk.Align.START;
					author.xalign = 0;
					author.justify = Gtk.Justification.LEFT;

					var body = new Gtk.Label("<big>" +
											 data.get_string_member("body") +
											 "</big>");
					body.use_markup = true;
					body.halign = Gtk.Align.START;
					body.xalign = 0;
					body.justify = Gtk.Justification.LEFT;
					body.wrap = true;
					comment_box.pack_start(body, false, false, 0);
					comment_box.pack_start(author, false, false, 0);
					comments_container.pack_start(comment_box, false, false,
												  COMMENT_SPACING);
				}
			});

		comments_container.show_all();
		// container.show_all();
	}
}