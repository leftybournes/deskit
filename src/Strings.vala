namespace Deskit.Strings {
	const string APPLICATION_NAME = "Deskit";
	const string APPLICATION_ID = "com.gitlab.kdeleteme.deskit";

	const string BASE_URL = "https://www.reddit.com";
}